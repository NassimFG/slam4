﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace SynapsInfo
{
    class Mission
    {

        private int _id;

        private string _nom;
        private string _description;
        private int _nbHeuresPrevues;
        private TimeSpan _releveHoraire;
        private Intervenant _executant;

        public TimeSpan getReleveHoraire
        {
            get
            {
                return _releveHoraire;

            }

             
        }

        public Intervenant getExecutant
        {
            get
            {
                return _executant;
            }
        }

        public void ajoutReleve(TimeSpan unJour, int unNbHeures)
        {
            
        }

        public Mission()
        {
            _id = -1;
        }

        #region Champs à Portée classe contenant l'ensemble des requêtes d'accès aux données

        private static string _selectSql = "SELECT id, nom, description, nbHeuresPrevues";

        private static string _selectByIdSql = "SELECT id, nom, description, nbHeuresPrevues, WHERE id = ?id";

        private static string _updateSql = "UPDATE mission SET nom = ?nom, description = ?description, nbHeuresPrevues = ?nbHeuresPrevues, WHERE id = ?id";

        private static string _insertSql = "INSERT INTO mission (nom, description, nbHeuresPrevues) VALUES (?nom, ?description, ?nbHeuresPrevues)";

        private static string _deleteByIdSql = "DELETE FROM mission where = id = ?id";

        private static string _getLastInsertId = "SELECT id FROM mission WHERE nom = ?nom AND description = ?description AND nbHeuresPrevues = ?nbHeuresPrevues";

        #endregion


        #region Méthode d'accès aux données

        public static Mission Fetch(int idMission)
        {
            Mission uneMission = null;
            DataBaseAccess.Connexion.Open();
            MySqlCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _selectByIdSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?id", idMission));
			commandSql.Prepare();
			MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
			bool existEnregistrement = jeuEnregistrements.Read();
			if (existEnregistrement)
			{
				uneMission = new Mission();
				uneMission._id = Convert.ToInt16(jeuEnregistrements["id"].ToString());
				uneMission._nom = jeuEnregistrements["nom"].ToString();
				uneMission._description = jeuEnregistrements["description"].ToString();
				uneMission._nbHeuresPrevues = Convert.ToInt16(jeuEnregistrements["nbHeuresPrevues"].ToString());
			}

			DataBaseAccess.Connexion.Close();
			return uneMission;

        }



		#endregion


		public void Save()
		{
			if (_id == -1)
			{
				Insert();
			}
			else
			{
				Update();
			}
		}


		public void Delete()
		{
			DataBaseAccess.Connexion.Open();
			MySqlCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
			commandSql.CommandText = _deleteByIdSql;
			commandSql.Parameters.Add(DataBaseAccess.CodeParam("?id", _id));
			commandSql.Prepare();
			commandSql.ExecuteNonQuery();
			_id = -1;
		}

		private void Update()
		{
			DataBaseAccess.Connexion.Open();
			MySqlCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
			commandSql.CommandText = _updateSql;
			commandSql.Parameters.Add(DataBaseAccess.CodeParam("?id", _id));
			commandSql.Parameters.Add(DataBaseAccess.CodeParam("?nom", _nom));
			commandSql.Parameters.Add(DataBaseAccess.CodeParam("?description", _description));
			commandSql.Parameters.Add(DataBaseAccess.CodeParam("?nbHeuresPrevues", _nbHeuresPrevues));
			commandSql.Prepare();
			commandSql.ExecuteNonQuery();
			DataBaseAccess.Connexion.Close();
		}

		private void Insert()
		{
			DataBaseAccess.Connexion.Open();
			MySqlCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
			commandSql.CommandText = _insertSql;
			commandSql.Parameters.Add(DataBaseAccess.CodeParam("?nom", _nom));
			commandSql.Parameters.Add(DataBaseAccess.CodeParam("?description", _description));
			commandSql.Parameters.Add(DataBaseAccess.CodeParam("?nbHeuresPrevues", _nbHeuresPrevues));
			commandSql.Prepare();
			commandSql.ExecuteNonQuery();

			MySqlCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
			commandGetLastId.CommandText = _getLastInsertId;
			commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?nom", _nom));
			commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?description", _description));
			commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?nbHeuresPrevues", _nbHeuresPrevues));
			commandGetLastId.Prepare();
			MySqlDataReader jeuEnregistrements = commandGetLastId.ExecuteReader();
			bool existEnregistrement = jeuEnregistrements.Read();
			if (existEnregistrement)
			{
				_id = Convert.ToInt16(jeuEnregistrements["id"].ToString());
			}
			else
			{
				commandSql.Transaction.Rollback();
				throw new Exception("Duplicate entry");
			}
			DataBaseAccess.Connexion.Close();
		}

		public static List<Mission> FetchAll()
		{
			List<Mission> resultat = new List<Mission>();
			DataBaseAccess.Connexion.Open();
			MySqlCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
			commandSql.CommandText = _selectSql;
			MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
			while (jeuEnregistrements.Read())
			{
				Mission mission = new Mission();
				string idMiss = jeuEnregistrements["id"].ToString();
				mission._id = Convert.ToInt16(idMiss);
				mission._nom = jeuEnregistrements["nom"].ToString();
				mission._description = jeuEnregistrements["description"].ToString();
				mission._nbHeuresPrevues = Convert.ToInt16(jeuEnregistrements["nbHeuresPrevues"].ToString());

				resultat.Add(mission);
			}
			DataBaseAccess.Connexion.Close();
			return resultat;
		}




    }
}
